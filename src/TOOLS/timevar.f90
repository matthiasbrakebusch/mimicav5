!
!------------------------------------------------------------------------!
! This file is part of MIMICA                                            !
!                                                                        !
! Copyright 2017-2021 Julien Savre                                       ! 
!                                                                        !
! This program is free software: you can redistribute it and/or modify   !
! it under the terms of the GNU General Public License as published by   !
! the Free Software Foundation, either version 3 of the License, or      !
! (at your option) any later version.                                    !
!                                                                        !
! This program is distributed in the hope that it will be useful,        !
! but WITHOUT ANY WARRANTY; without even the implied warranty of         !
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          !
! GNU General Public License for more details.                           !
!                                                                        !
! You should have received a copy of the GNU General Public License      !
! along with this program.  If not, see <http://www.gnu.org/licenses/>.  !
!------------------------------------------------------------------------!
!

#include "ctrparam.h"

! ================================================================
!
!  TIMEVAR:
!	Updates potential time varying quantities
!
!  Author:
!	Julien Savre, MISU
!
! ================================================================	 
!
!  ===================================================
   subroutine timevar
!  ===================================================
!
  USE gridno
  USE shared_data
  USE SE_atlantic
  USE comble
!
IMPLICIT NONE
!
  if (verbose > 0) call write_debug('Starting timevar')
!
!----------------------------------------------------------!
!    Call functions to calculate time varying variables    !
!----------------------------------------------------------!
!
  if ( with_dif ) call time_surf 
!
  if ( with_lsadv ) call time_lsadv
!
  if (trim(casename) == 'SE_ATLANTIC') call se_atlantic_forcing 
!
  if (trim(casename) == 'COMBLE') call comble_forcing
!----------------------------------------------------------!
!
  if (verbose > 0) call write_debug('Terminating timevar')
!
return
end
!
!
!  ===================================================
   subroutine time_surf
!  ===================================================

USE gridno
USE shared_data
!
IMPLICIT NONE
!
  real :: ft
  logical, save :: lfirst=.true.
!
!----------------------------------------------------------!
!               Time varying surface fluxes:               !
!         Expressed as a linear function of time           !
!----------------------------------------------------------!
!
  select case (trim(casename))
!
    case ('TRANSIT')
      ft = max(0.,cos(0.5*pi*(5.25 - time/3600. + t_local)/5.25))
      shf = shf0 * (ft)**1.5
      lhf = lhf0 * (ft)**1.3
!
    case ('GERMANY')
      ft = max(0.,sin(pi*(time/3600. + t_local - 6.)/14.))
      shf = shf0*ft**6.5 - 10.
      lhf = lhf0*ft**6.5 + 10.
!
    case ('MIDLAT')
      ft = max(0.,sin(pi*(time/3600. + t_local - 6.)/12.))
      shf = shf0*ft
      lhf = lhf0*ft
!
    case default
      shf = shf0
      lhf = lhf0
!
  end select   
!
return
end
!
!
!  ===================================================
   subroutine time_lsadv
!  ===================================================

#ifdef SPMD
USE mpi
#endif
USE gridno
USE shared_data
!
IMPLICIT NONE
!
  logical  :: ladv=.false.
  integer  :: ierr, it
!
!----------------------------------------------------------!
!           Time varying large scale adveection            !
!----------------------------------------------------------!
!
  lsp = 0.0
  lsqt = 0.0
  lstem = 0.0
!
  select case (casename)
!
  case ('ISDAC')
  ladv = .true.
  it = time/10800.
!
  if (mypid == 0) then
    lsp(1) = 1.e+3*psurf**(cp/Ra)
    lsp(2) = 975.
    lsp(3) = 950.
    lsp(4) = 925.
    lsp(5) = 900.
    lsp(6) = 875.
    lsp(7) = 850.
    lsp(8) = 825.
!
!    if (it == 0) then
      lsqt0(1)   = 0.2e-7
      lsqt0(2)   = 0.2e-7
      lsqt0(3)   = 0.2e-7
      lsqt0(4)   = 0.2e-7
      lsqt0(5)   = 0.2e-7
      lsqt0(6)   = 0.2e-7
      lsqt0(7)   = 0.2e-7
      lsqt0(8)   = 0.2e-7
      lstem0(1) = 0.
      lstem0(2) = 0.
      lstem0(3) = 0.
      lstem0(4) = 0.
      lstem0(5) = 0.
      lstem0(6) = 0.
      lstem0(7) = 0.
      lstem0(8) = 0.   
!
      lsqt(1)   = 0.2e-7
      lsqt(2)   = 0.2e-7
      lsqt(3)   = 0.2e-7
      lsqt(4)   = 0.2e-7
      lsqt(5)   = 0.2e-7
      lsqt(6)   = 0.2e-7
      lsqt(7)   = 0.2e-7
      lsqt(8)   = 0.2e-7
      lstem(1) = 0.
      lstem(2) = 0.
      lstem(3) = 0.
      lstem(4) = 0.
      lstem(5) = 0.
      lstem(6) = 0.
      lstem(7) = 0.
      lstem(8) = 0.	
!    else if (it == 1) then
!      lsqt0  = lsqt
!      lstem0 = lstem
!      lsqt(1)   = 0.03e-7
!      lsqt(2)   = 0.06e-7
!      lsqt(3)   = 0.1e-7
!      lsqt(4)   = 0.13e-7
!      lsqt(5)   = 0.17e-7
!      lsqt(6)   = 0.17e-7
!      lsqt(7)   = 0.17e-7
!      lsqt(8)   = 0.17e-7
!      lstem(1) = 0.
!      lstem(2) = 0.
!      lstem(3) = 0.
!      lstem(4) = 0.
!      lstem(5) = 0.
!      lstem(6) = 0.
!      lstem(7) = 0.
!      lstem(8) = 0.	   
!    else if (it == 2) then
!      lsqt0  = lsqt
!      lstem0 = lstem
!      lsqt(1)   = 0.03e-7
!      lsqt(2)   = 0.06e-7
!      lsqt(3)   = 0.1e-7
!      lsqt(4)   = 0.13e-7
!      lsqt(5)   = 0.17e-7
!      lsqt(6)   = 0.17e-7
!      lsqt(7)   = 0.17e-7
!      lsqt(8)   = 0.17e-7
!      lstem(1) = 0.
!      lstem(2) = 0.
!      lstem(3) = 0.
!      lstem(4) = 0.
!      lstem(5) = 0.
!      lstem(6) = 0.
!      lstem(7) = 0.
!      lstem(8) = 0.	   
!    endif 
  endif
!  
  end select
!
!  Broadcast new large scale tendencies
!
#ifdef SPMD
  if (ladv) then
    call MPI_BCAST(lsp,      8, REALTYPE,0,MPI_COMM_WORLD,ierr)  
    call MPI_BCAST(lsqt,     8, REALTYPE,0,MPI_COMM_WORLD,ierr)      
    call MPI_BCAST(lsqt0,    8, REALTYPE,0,MPI_COMM_WORLD,ierr)
    call MPI_BCAST(lstem,    8, REALTYPE,0,MPI_COMM_WORLD,ierr)      
    call MPI_BCAST(lstem0,   8, REALTYPE,0,MPI_COMM_WORLD,ierr)
  endif
#endif    
!
return
end
!
!
!  ===================================================
   subroutine add_pert 
!  ===================================================

USE gridno
USE shared_data
USE shared_state
#ifdef SPMD
USE mpicomm
#endif
!
IMPLICIT NONE
!
  integer :: i, j, ii, jj, ii1, jj1, k, ni, nj
  INTEGER, DIMENSION (8) :: T
  REAL :: rr,p,x,RANF 
  REAL :: xx(nx), yy(ny)
  INTEGER :: SEED      
!
  real, dimension(nx,ny) :: pos
  real, dimension(ip_start:ip_end,jp_start:jp_end) :: pos_l
!
  real, parameter :: d_pt = 2.5, xr = 1500., yr = 1500.
!
!----------------------------------------------------------!
!               Add pseudo-random perturbations            !
!----------------------------------------------------------!
!
  select case (trim(casename))
!
    case ('MIDLAT')
!
!  Initialisations
!
      if (mypid == 0) then
        CALL DATE_AND_TIME(VALUES = T)
        SEED = 43*mypid+7*T(1)+70*(T(2)+12*(T(3)+31*(T(5)+23*(T(6)+59*T(7)))))
        IF (MOD(SEED,2).EQ.0) SEED = SEED-1
!
        do i = 1, nx
          xx(i) = real(i-1)*dx
        enddo
        do j = 1, ny
          yy(j) = real(j-1)*dy
        enddo
        ni = int(xr/dx)+1
        nj = int(yr/dy)+1
!
!  Define randomly distributed bubbles in plane
!
        pos = 0.
        p = 800. / real((nx-5)*(ny-5))
        do j = 4, ny-2
          do i = 4, nx-2
            x = RANF(SEED)
            if (x < p) then
              do jj = j-nj, j+nj
                jj1 = jj
                if (jj1 > ny-2) jj1 = jj1 - (ny-5)
                if (jj1 < 4) jj1 = jj1 + (ny-5)
                do ii = i-ni, i+ni
                  ii1 = ii
                  if (ii1 > nx-2) ii1 = ii1 - (nx-5)
                  if (ii1 < 4) ii1 = ii1 + (nx-5)
 	          rr = sqrt( ((xx(ii1) - xx(i))/xr)**2. + ((yy(jj1) - yy(j))/yr)**2. )
	          if (rr < 1.) pos(ii1,jj1) = pos(ii1,jj1) + 0.5*(1. + cos(pi*rr))
                enddo
              enddo
            endif
          enddo
        enddo
      endif
!
#ifdef SPMD
      call distribute(pos_l, pos)
#else
      pos_l = pos
#endif
!
!  Perturb potential temperature field
!
      do k = 11, 27	!between 300m and 800m
        do j = jp_start, jp_end
          do i = ip_start, ip_end
            state2%es(i,j,k) = state2%es(i,j,k) + d_pt*pos_l(i,j) 
          enddo
        enddo
      enddo
!
  end select
!
return 
end
