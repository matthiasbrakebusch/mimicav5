#include "ctrparam.h"
!
!
  module SE_atlantic

!  
!  ---------------------------------------------
!
!  General modules
!
  USE gridno 
  USE shared_data    
  USE shared_all
  USE averages
  USE gradients

  IMPLICIT NONE

  private 
  
  integer, parameter :: aero_cloud_dist=2, aero_layer_depth=10
  real, parameter :: aero_time=7200.
   
  public :: se_atlantic_forcing  

  contains
!
!  ===================================================
subroutine se_atlantic_forcing
!  ===================================================
!
!  call time_aerosol
!
!  call time_moisture
!
  call forcing_SE_Atlantic_New
!
return
end
!
!  ===================================================
subroutine time_aerosol
!  ===================================================

IMPLICIT NONE

  real*8,dimension(1:nz) :: gradz_PT_sum
  integer :: im, k, Zmax_PT_grad !level of maximum vertical potential temperature gradient
  integer :: aero_base,lev_ave_value 
  real :: d_time !time period during which the aero3d2%n increases
  real*8 :: aero_n0,slope

#ifdef AERO_ENABLE
  !do k = 1, nz
  !do k = 239, 278    
  !  do im = 1, nmode
      
        !aero3d2(im)%n(:,:,k)  = aero0(im)%n(k)+0.5*aero0(im)%n(k)*sin(0.01*time)
        !aero3d2(im)%m(:,:,k)  = aero0(im)%m(k)+0.5*aero0(im)%m(k)*sin(0.01*time)
        !aero3d2(im)%ma(:,:,k) = 0.                
    !end do
  !end do
  
  d_time=1000.
  aero_n0=3.087e8
  slope=1.5e6 
  lev_ave_value=275

  if (nite>=2) then

    call horav(diag(3)%grad%pt,gradz_PT_sum)
    Zmax_PT_grad = SUM(maxloc(gradz_PT_sum(1:248)))
    aero_base = Zmax_PT_grad + aero_cloud_dist


    print*, 'time=',time,'mypid=',mypid
    print*, 'subroutine time aerosol'
    print*, 'Zmax_PT_grad=',Zmax_PT_grad
    print*, 'aero_base=',aero_base


    do im=1,nmode
        FORALL(k=Zmax_PT_grad:lev_ave_value) aero3d2(im)%n(:,:,k)= aero_n0
        FORALL(k=Zmax_PT_grad:lev_ave_value) aero3d2(im)%m(:,:,k)= aero(im)%size%mmean*aero3d2(im)%n(:,:,k)
    enddo
  
  end if 

  if ((time>=aero_time).and.(time<= aero_time+d_time)) then
    do im=1, nmode
        !Aerosol layer (increasing aerosol number concentration)
        FORALL(k=aero_base:aero_base+aero_layer_depth) aero3d2(im)%n(:,:,k)= aero_n0+slope*(time-aero_time)       
        FORALL(k=aero_base:aero_base+aero_layer_depth) aero3d2(im)%m(:,:,k)= aero(im)%size%mmean*aero3d2(im)%n(:,:,k) 
    enddo

  endif

  if (time > aero_time+d_time) then
    do im=1,nmode
        !Aerosol layer (constant aerosol number concentration)
        FORALL(k=aero_base:aero_base+aero_layer_depth) aero3d2(im)%n(:,:,k)= aero_n0+slope*d_time       
        FORALL(k=aero_base:aero_base+aero_layer_depth) aero3d2(im)%m(:,:,k)= aero(im)%size%mmean*aero3d2(im)%n(:,:,k) 
    enddo
  endif

  if (verbose > 2) call write_debug('Finishing subroutine time_aerosol ')

#endif

return
end

!  ===================================================
subroutine time_moisture
!  ===================================================

 IMPLICIT NONE

 real*8,dimension(1:nz) :: gradz_PT_sum
 integer :: k, Zmax_PT_grad !level of maximum vertical potential temperature gradient
 integer :: moist_base,lev_ave_value 
 real :: d_time !time period during which the state2%qt increases
 real*8 :: qt_ave0,slope

 d_time=1000.
 qt_ave0=3.e-3 
 slope=4.e-6 
 lev_ave_value=275

  if (nite>=2) then

    call horav(diag(3)%grad%pt,gradz_PT_sum)
    Zmax_PT_grad = SUM(maxloc(gradz_PT_sum(1:248)))
    moist_base = Zmax_PT_grad + aero_cloud_dist

    FORALL( k= Zmax_PT_grad:lev_ave_value) state2%qt(:,:,k)= qt_ave0

  endif

 if ((time>=aero_time).and.(time<= aero_time+d_time)) then
  !Moisture layer (increasing total water mixing ratio qt0)
  FORALL(k=moist_base:moist_base+aero_layer_depth) state2%qt(:,:,k)= qt_ave0+slope*(time-aero_time)       
 endif

 if (time > aero_time+d_time) then
    !Moisture layer (constant total water mixing ratio)
    FORALL(k=moist_base:moist_base+aero_layer_depth) state2%qt(:,:,k)= qt_ave0+slope*d_time        
 endif

  return
  end


!======================================================
subroutine forcing_SE_Atlantic_New
!=======================================================
  
  IMPLICIT NONE

  integer :: ii,jj, time_simulation ,k,im 
  integer :: nudge_base,lev_max
  real*8,dimension(ip_start:ip_end,jp_start:jp_end,1:nz) :: grad_PT_matrix
  real*8,dimension(1:nz) :: gradz_PT_sum,zdiff!,coef_nudge1,coef_nudge2,
  real,dimension(nz) :: coef1!,coef2
  real*8,dimension(1:nz) :: Na_force_horiz_ave 
  integer,dimension(3) :: Zmax_PT_grad_loc,Zmax_PT_grad_loc1,Zmax_PT_grad_loc2  !location of maximum vertical potential temperature gradient 
  integer ::Zmax_PT_grad,Zmax_PT_grad1,Zmax_PT_grad2!level of maximum vertical potential temperature gradient
  integer :: tau_nudge1,tau_nudge2,tau_gap_ft,row

  if (verbose > 2) call write_debug('Starting subroutine forcing_SE_Atlantic ')
  
  lev_max=nz            !Highest level to force MIMICA = number of total levels

  tau_gap_ft= 200
  tau_nudge1 =1800 !for PT/qt/Na 
  tau_nudge2= 10800 !for  u/v

  do jj = jt_start, jt_end
    do ii = it_start, it_end
      call grad_1d ( state2%es(ii,jj,:), grad_PT_matrix(ii,jj,:) )    
    enddo
  enddo
  Zmax_PT_grad_loc=maxloc(grad_PT_matrix) 
  Zmax_PT_grad=Zmax_PT_grad_loc(3)

  if (nite+1 > 2) then
   zbl = z0(Zmax_PT_grad) !Modifiying default value of zbl from cm.nml
  endif

  !Lowest level vertical level to force the model:
  !Maximum vertical potential temperature gradient + 100m
  nudge_base=SUM(minloc(abs(z0-(z0(Zmax_PT_grad)+100))))     

  !coef_nudge is initialized with value =0
  coef_nudge1=0
  coef_nudge2=0
  zdiff=0

  do k=1,nz

    if(z0(k) - z0(nudge_base) >= 0 .and. z0(k) - z0(nudge_base)< tau_gap_ft) then    
      coef_nudge1(k) = 1.0 / tau_nudge1 * 0.5 * ( 1.0 - cos( pi * ( z0(k) - z0(nudge_base) ) / tau_gap_ft ) )
      coef_nudge2(k) = 1.0 / tau_nudge2 * 0.5 * ( 1.0 - cos( pi * ( z0(k) - z0(nudge_base) ) / tau_gap_ft ) )  

      zdiff(k)=z0(k)-z0(nudge_base)

    else if (z0(k) - z0(nudge_base) >= tau_gap_ft) then 
      coef_nudge1(k) = 1.0 / tau_nudge1
      coef_nudge2(k) = 1.0 / tau_nudge2

      zdiff(k)=z0(k)-z0(nudge_base) 
    endif
  enddo

  coef1=dt*coef_nudge1
  !coef2=dt*coef_nudge2
  
  !Index of the time that will be used in the forcing file.  
  time_simulation=1+(time/3600) 

  do im=1,nmode

    !Computing horizontal average of the aerosol number concentration for each aerosol mode
    call horav(aero3d2(im)%n,Na_force_horiz_ave)

    !Values above the buffer region = values in the forcing file   
    FORALL(k=nudge_base:lev_max) aero3d2(im)%n(:,:,k)= aero3d2(im)%n(:,:,k)- (Na_force_horiz_ave(k)-1e6*Na_force(lev_len-k+1,time_simulation))&
                                                     *coef1(k)    
  
    !Aerosol mass concentration is computed directly from the aerosol number concentration
    FORALL(k=nudge_base:lev_max) aero3d2(im)%m(:,:,k)= aero(im)%size%mmean*aero3d2(im)%n(:,:,k) 
  enddo

  FORALL (k=1:nz) ta_force(k)=ta_nud(lev_len-k+1,time_simulation)
  FORALL (k=1:nz) qt_force(k)=qt_nud(lev_len-k+1,time_simulation)
  FORALL (k=1:nz) u_force(k)=u_nud(lev_len-k+1,time_simulation)
  FORALL (k=1:nz) v_force(k)=v_nud(lev_len-k+1,time_simulation) 

  !Forcing surface temperature:
   
  !Using linear interpolation formula:
  !y = (x-x0) (y1-y0)/(x1-x0) + y0

  !x0 = 0     
  !x1 = 3600 (1hour)
  !y0 = value of the variable at time (x0)
  !y1 = value of the variable at time (x1)

  !x = time of the simulation
  !y  = value of the variable at time of the simulation
  
  sst = MOD(time,3600.)* (ts_force(time_simulation+1) - ts_force(time_simulation))/3600. + ts_force(time_simulation) 

  if (verbose > 2) call write_debug('Finishing subroutine forcing_SE_Atlantic ')
  
  return
end

end module SE_atlantic
