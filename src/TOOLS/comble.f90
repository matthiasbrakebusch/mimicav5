#include "ctrparam.h"

module comble

!  General modules
!
    USE gridno 
    USE shared_data    
    USE shared_all
    USE averages
    USE gradients
  
    IMPLICIT NONE

    public :: comble_forcing 
    
    contains

    !  ===================================================
     subroutine comble_forcing
    !  ===================================================
    !
      !call forcing_COMBLE_time    !
      call forcing_COMBLE_time_new
    return
    end

    !======================================================
    subroutine forcing_COMBLE_time
    !=======================================================
      USE shared_state
      USE averages
      USE gridno
      USE shared_thermo
      
      IMPLICIT NONE
    
      integer :: time_simulation
  
      real,parameter ::  ts_COMBLE(21) = (/247.      , 247.      , 249.935195, 271.085547, 275.382812, 275.181641, & 
                                         & 275.460938, 275.802734, 276.263672, 276.55957 , 276.777344, 277.299805, &  
                                         & 277.844727, 277.981445, 278.177734, 278.541016, 279.149414, 279.380859, &
                                         & 279.43457 , 279.323242, 277.790039/)
  
      if (verbose > 0) call write_debug('Starting subroutine forcing_COMBLE')
  
      !Index of the time that will be used in the forcing file.  
      time_simulation=1+(time/3600) 
    
      if (verbose > 0) write(7,*) 'time_simulation=',time_simulation
    
      !Forcing surface temperature:
       
      !Using linear interpolation formula:
      !y = (x-x0) (y1-y0)/(x1-x0) + y0
    
      !x0 = 0     
      !x1 = 3600 (1hour)
      !y0 = value of the variable at time (x0)
      !y1 = value of the variable at time (x1)
    
      !x = time of the simulation
      !y  = value of the variable at time of the simulation
  
      sst = MOD(time,3600.)* (ts_COMBLE(time_simulation+1) - ts_COMBLE(time_simulation))/3600. + ts_COMBLE(time_simulation) 
    
      if (verbose > 0) write(7,*) 'sst =', sst
    
      if (verbose > 0) call write_debug('Finishing subroutine forcing_COMBLE')

      
      return
      end

    !======================================================
    subroutine forcing_COMBLE_time_New
    !=======================================================
      USE shared_state
      USE averages
      USE gridno
      USE shared_thermo
          
        IMPLICIT NONE
      
        integer :: ii,jj, time_simulation ,k,im 
        integer :: nudge_base,lev_max
        real*8,dimension(ip_start:ip_end,jp_start:jp_end,1:nz) :: grad_PT_matrix
        real*8,dimension(1:nz) :: gradz_PT_sum,zdiff!,coef_nudge1,coef_nudge2,
        real,dimension(nz) :: coef1!,coef2
        integer :: tau_nudge1,tau_nudge2,tau_gap_ft,row
      
        if (verbose > 2) call write_debug('Starting subroutine forcing_COMBLE')
                
        !Index of the time that will be used in the forcing file.  
        time_simulation=1+(time/3600) 

        !Interpolating linearly in time the hourly forcing values of 
        !geostrophic wind (ug_force,vg_force) and surface temperature (SST):
          
        !Using linear interpolation formula:
        !y = (x-x0) (y1-y0)/(x1-x0) + y0
      
        !x0 = 0     
        !x1 = 3600 (1hour)
        !y0 = value of the variable at time (x0)
        !y1 = value of the variable at time (x1)
      
        !x = time of the simulation
        !y  = value of the variable at time of the simulation
      
        FORALL (k=1:nz) ug_force(k)=MOD(time,3600.)* (ug_nud(k,time_simulation+1) - ug_nud(k,time_simulation))/3600. + ug_nud(k,time_simulation) 
        FORALL (k=1:nz) vg_force(k)=MOD(time,3600.)* (vg_nud(k,time_simulation+1) - vg_nud(k,time_simulation))/3600. + vg_nud(k,time_simulation) 

        !FORALL (k=1:nz) ug_force(k)=ug_nud(k,time_simulation)
        !FORALL (k=1:nz) vg_force(k)=vg_nud(k,time_simulation) 
      
  
        sst = MOD(time,3600.)* (ts_force(time_simulation+1) - ts_force(time_simulation))/3600. + ts_force(time_simulation) 
    
        if (verbose > 0) call write_debug('Finishing subroutine forcing_COMBLE')
        
        return
      end

    end module comble
