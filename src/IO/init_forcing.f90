!
!------------------------------------------------------------------------!
! This file is part of MIMICA                                            !
!                                                                        !
! Copyright 2017-2021 Julien Savre                                       ! 
!                                                                        !
! This program is free software: you can redistribute it and/or modify   !
! it under the terms of the GNU General Public License as published by   !
! the Free Software Foundation, either version 3 of the License, or      !
! (at your option) any later version.                                    !
!                                                                        !
! This program is distributed in the hope that it will be useful,        !
! but WITHOUT ANY WARRANTY; without even the implied warranty of         !
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          !
! GNU General Public License for more details.                           !
!                                                                        !
! You should have received a copy of the GNU General Public License      !
! along with this program.  If not, see <http://www.gnu.org/licenses/>.  !
!------------------------------------------------------------------------!
!

#include "ctrparam.h"

! ================================================================
!
!  init_forcing.f90:                   
!
!  Purpose:
!      Initializes external forcings from file for specific case:
!          - SE_Atlantic
!          - COMBLE
!      
!  Author
!      Julien Savre and Alejandro Baro Perez
!            
! ================================================================

      module init_forcing

      USE netcdf
      USE shared_data 
      USE shared_all   
      USE shared_aerosol_new
      USE shared_wind
      USE shared_state
      USE gridno
      USE shared_thermo
!
#ifdef SPMD
      USE mpi
#endif
      
      IMPLICIT NONE

      private

      public :: forcings 

      CONTAINS

     !======================================================
      subroutine forcings
     !=====================================================
!
      if (verbose > 2) call write_debug('Starting forcings')
!
!  Read forcings from files
!
      if (mypid == 0) then
!
        select case ( trim(casename) )
       
        case ('COMBLE')
!
          call read_forcing_COMBLE
!
        case ('SE_ATLANTIC')
!
          call read_forcing_SE_Atlantic
!
          call forcing_SE_Atlantic_time1
!     
        end select  
!
      endif
!
!  Broadcasting to all procs
!
#if ( defined SPMD )
      call MPI_BCAST_Forcing
#endif
!
      if (verbose > 2) call write_debug('Terminating forcings')
      
      return
      end

     !======================================================
      subroutine forcing_SE_Atlantic_time1
     !=====================================================
      
      integer :: time_simulation, k, t 
      
      time_simulation=1

      !nz-k+1 is used to reverse the order of the arrays that are
      FORALL (k=1:nz) t0(k)=ta_nud(lev_len-k+1,time_simulation)
      FORALL (k=1:nz) qv0(k)=qt_nud(lev_len-k+1,time_simulation)
      FORALL (k=1:nz) u00(k)=u_nud(lev_len-k+1,time_simulation)
      FORALL (k=1:nz) v00(k)=v_nud(lev_len-k+1,time_simulation) 
      
      !ta_nud (absolute temperature) is converted to potential temperature
      FORALL(k=1:lev_len,t=1:time_len) ta_nud(k,t) = ta_nud(k,t)*((pref/pa_force(k,t))&
                                                      **((cp_a-cv_a)/cp_a))
      
      return
      end

      !======================================================
      subroutine read_forcing_SE_Atlantic
      !=======================================================
      
      integer :: ii, ncid, status, time_simulation, k, im, ierr
      integer :: time_id,lat_id,lon_id,lev_id
      integer :: lat_len,lon_len 
      integer :: tsec_id, Na_accum_id, q_id
      integer :: ta_id, pa_id, ts_id, ps_id
      integer :: wind_u_id, wind_v_id
      
      if (verbose > 2) call write_debug ('Starting read_forcing_SE_Atlantic')
      
      !Opening file with atmospheric conditions to force the model.
      call check(NF90_OPEN(forcing_file,NF90_NOWRITE,ncid))
      
      !getting the dimension ids 
      call check(NF90_INQ_DIMID(ncid,"time",time_id))  
      call check(NF90_INQ_DIMID(ncid,"zh",lev_id))    
      call check(NF90_INQ_DIMID(ncid,"lat",lat_id))
      call check(NF90_INQ_DIMID(ncid,"lon",lon_id))
      
      !getting the dimension lengths
      call check(NF90_INQUIRE_DIMENSION(ncid,time_id,len=time_len))
      call check(NF90_INQUIRE_DIMENSION(ncid,lev_id,len=lev_len))  
      call check(NF90_INQUIRE_DIMENSION(ncid,lat_id,len=lat_len))
      call check(NF90_INQUIRE_DIMENSION(ncid,lon_id,len=lon_len)) 
      
      !- Allocation of variables : 
      ALLOCATE(  Na_force(lev_len,time_len))  
      ALLOCATE(  qt_nud(lev_len,time_len)) 
      ALLOCATE(  ta_nud(lev_len,time_len))
      ALLOCATE(  pa_force(lev_len,time_len))  
      ALLOCATE(  u_nud(lev_len,time_len))
      ALLOCATE(  v_nud(lev_len,time_len))
      ALLOCATE(  ts_force(time_len))
      ALLOCATE(  ps_force(time_len))
            
      !Getting the variables ids 
      call check(NF90_INQ_VARID(ncid,"na_nud",Na_accum_id))
      call check(NF90_INQ_VARID(ncid,"qt_nud",q_id))
      call check(NF90_INQ_VARID(ncid,"ta_nud",ta_id))  
      call check(NF90_INQ_VARID(ncid,"pa_force",pa_id)) 
      call check(NF90_INQ_VARID(ncid,"ua_nud",wind_u_id))
      call check(NF90_INQ_VARID(ncid,"va_nud",wind_v_id))
      call check(NF90_INQ_VARID(ncid,"ts_force",ts_id))
      call check(NF90_INQ_VARID(ncid,"ps_force",ps_id))  
            
      !Reading the data
      call check(NF90_GET_VAR(ncid,Na_accum_id,Na_force))  
      call check(NF90_GET_VAR(ncid,q_id,qt_nud))  
      call check(NF90_GET_VAR(ncid,ta_id,ta_nud))
      call check(NF90_GET_VAR(ncid,pa_id,pa_force))  
      call check(NF90_GET_VAR(ncid,wind_u_id,u_nud))
      call check(NF90_GET_VAR(ncid,wind_v_id,v_nud))
      call check(NF90_GET_VAR(ncid,ts_id,ts_force))
      call check(NF90_GET_VAR(ncid,ps_id,ps_force))  
      
      !closing file
      CALL check(nf90_close(ncid))

      if (verbose > 2) call write_debug ('Terminating read_forcing_SE_Atlantic')

      return
      end

      !======================================================
      subroutine read_forcing_COMBLE
      !=======================================================
            
            integer :: ii, ncid, status, time_simulation, k, im, ierr
            integer :: time_id,lat_id,lon_id,lev_id
            integer :: lat_len,lon_len 
            integer :: tsec_id 
            integer :: ta_id, pa_id, ts_id, ps_id
            integer :: wind_u_id, wind_v_id
            
            if (verbose > 2) call write_debug ('Starting read_forcing_COMBLE')
            
            !Opening file with atmospheric conditions to force the model.
            call check(NF90_OPEN(forcing_file,NF90_NOWRITE,ncid))
            
            !getting the dimension ids 
            call check(NF90_INQ_DIMID(ncid,"time",time_id))  
            call check(NF90_INQ_DIMID(ncid,"lev",lev_id))    
            
            !getting the dimension lengths
            call check(NF90_INQUIRE_DIMENSION(ncid,time_id,len=time_len))
            call check(NF90_INQUIRE_DIMENSION(ncid,lev_id,len=lev_len))  
            
            !- Allocation of variables :    
            ALLOCATE(  ug_nud(lev_len,time_len))
            ALLOCATE(  vg_nud(lev_len,time_len))
            ALLOCATE(  ts_force(time_len))
                  
            !Getting the variables ids 
            call check(NF90_INQ_VARID(ncid,"ug",wind_u_id))
            call check(NF90_INQ_VARID(ncid,"vg",wind_v_id))
            call check(NF90_INQ_VARID(ncid,"ts",ts_id))
      
            !Reading the data  
            call check(NF90_GET_VAR(ncid,wind_u_id,ug_nud))
            call check(NF90_GET_VAR(ncid,wind_v_id,vg_nud))
            call check(NF90_GET_VAR(ncid,ts_id,ts_force)) 
            
            !closing file
            CALL check(nf90_close(ncid))
      
            if (verbose > 2) call write_debug ('Terminating read_forcing_COMBLE')
      
            return
            end

  !===============================================

  SUBROUTINE check(istatus)
  !!Checks the return status of a nf90 function. If an error
  !!occured, the according error message is written to the
  !!screen and the program exits
    
  INTEGER, INTENT (IN) :: istatus

  IF (istatus /= nf90_noerr) THEN
     WRITE(*,*) TRIM(ADJUSTL(nf90_strerror(istatus)))
     WRITE(*,*) 'exiting'
     STOP 
  END IF
  END SUBROUTINE check
!
  subroutine MPI_BCAST_Forcing

  integer :: ierr
        
  select case (trim(casename) )
  case('SE_ATLANTIC')
    call MPI_BCAST(lev_len,    1, INTTYPE,0,MPI_COMM_WORLD,ierr)
    call MPI_BCAST(time_len,   1, INTTYPE,0,MPI_COMM_WORLD,ierr)   
    
    if (mypid /= 0) then
      !- Allocation of variables : 
      ALLOCATE(  Na_force(lev_len,time_len))  
      ALLOCATE(  qt_nud(lev_len,time_len)) 
      !ALLOCATE(  PT(lev_len,time_len)) 
      ALLOCATE(  ta_nud(lev_len,time_len))
      ALLOCATE(  pa_force(lev_len,time_len))  
      ALLOCATE(  u_nud(lev_len,time_len))
      ALLOCATE(  v_nud(lev_len,time_len))
      !ALLOCATE(  w_nud(lev_len,time_len))
      ALLOCATE(  ts_force(time_len))
      ALLOCATE(  ps_force(time_len))
    endif

    call MPI_BCAST(Na_force,   lev_len*time_len, REALTYPE,0,MPI_COMM_WORLD,ierr)            
    call MPI_BCAST(ta_nud,     lev_len*time_len, REALTYPE,0,MPI_COMM_WORLD,ierr) 
    call MPI_BCAST(qt_nud,     lev_len*time_len, REALTYPE,0,MPI_COMM_WORLD,ierr) 
    call MPI_BCAST(pa_force,   lev_len*time_len, REALTYPE,0,MPI_COMM_WORLD,ierr)                           
    call MPI_BCAST(u_nud,      lev_len*time_len, REALTYPE,0,MPI_COMM_WORLD,ierr)
    call MPI_BCAST(v_nud,      lev_len*time_len, REALTYPE,0,MPI_COMM_WORLD,ierr)  
    call MPI_BCAST(ts_force,   time_len, REALTYPE,0,MPI_COMM_WORLD,ierr)
    call MPI_BCAST(ps_force,   time_len, REALTYPE,0,MPI_COMM_WORLD,ierr)  

  case('COMBLE')
    call MPI_BCAST(lev_len,    1, INTTYPE,0,MPI_COMM_WORLD,ierr)
    call MPI_BCAST(time_len,   1, INTTYPE,0,MPI_COMM_WORLD,ierr)   

    if (mypid /= 0) then
      !- Allocation of variables : 
      ALLOCATE(  ug_nud(lev_len,time_len))
      ALLOCATE(  vg_nud(lev_len,time_len))
      ALLOCATE(  ts_force(time_len))
    endif

    call MPI_BCAST(ug_nud,      lev_len*time_len, REALTYPE,0,MPI_COMM_WORLD,ierr)
    call MPI_BCAST(vg_nud,      lev_len*time_len, REALTYPE,0,MPI_COMM_WORLD,ierr)  
    call MPI_BCAST(ts_force,   time_len, REALTYPE,0,MPI_COMM_WORLD,ierr)  

  end select
  end

  end module init_forcing
